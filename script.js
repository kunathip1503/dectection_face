const video = document.getElementById("video");
let canvas = null;
let displaySize = null;

async function startVideo() {
  await Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri("/models"),
    faceapi.nets.faceLandmark68Net.loadFromUri("/models"),
    faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
    faceapi.nets.faceExpressionNet.loadFromUri("/models"),
  ]);

  navigator.getUserMedia(
    { video: {} },
    (stream) => (video.srcObject = stream),
    (err) => console.error(err)
  );
}

video.addEventListener("loadedmetadata", () => {
  canvas = faceapi.createCanvasFromMedia(video);
  document.body.append(canvas);
  displaySize = { width: video.width, height: video.height };
  faceapi.matchDimensions(canvas, displaySize);
});

video.addEventListener("play", () => {
  const detectionOptions = new faceapi.TinyFaceDetectorOptions({
    inputSize: 128,
  });

  async function detectFaces() {
    const detections = await faceapi
      .detectAllFaces(video, detectionOptions)
      .withFaceLandmarks()
      .withFaceExpressions();

    const resizedDetections = faceapi.resizeResults(detections, displaySize);
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    faceapi.draw.drawDetections(canvas, resizedDetections);
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections);
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections);

    requestAnimationFrame(detectFaces);
  }

  requestAnimationFrame(detectFaces);
});

startVideo();
